<?php
// $Id$

/**
 * @file
 * Administer the Probe module
 */

function probe_admin_settings() {

  $form['probe_probe_url'] = array(

    '#type' => 'textfield',
    '#title' => t('URL of probe page'),
    '#required' => FALSE,
    '#default_value' => variable_get('probe_probe_url', 'probe.php')
  );

  /*
  $form['probe_status_url'] = array(

    '#type' => 'textfield',
    '#title' => t('URL of status page'),
    '#required' => FALSE,
    '#default_value' => variable_get('probe_status_url', 'status.php')
  );
  */

  $form['probe_enable_checks'] = array(

    '#type' => 'checkbox',
    '#title' => t('Enable additional checks'),
    '#required' => FALSE,
    '#default_value' => variable_get('probe_enable_checks', TRUE)
  );

  //Invoke hook_probe in modules which provide probe checks
  $probe_checks = module_invoke_all('probe_check');

  $form['probe_additional_checks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional checks'),
    '#collapsible' => TRUE
  );

  foreach ($probe_checks as $probe_check) {

    $form['probe_additional_checks']['probe_enable_check_'.$probe_check['name']] = array(

      '#type' => 'checkbox',
      '#title' => $probe_check['name'],
      '#description' => $probe_check['description'],
      '#required' => FALSE,
      '#default_value' => variable_get('probe_enable_check_'.$probe_check['name'], TRUE)
    );
  }

  $form['#submit'][] = 'probe_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Implementation of hook_submit().
 */
function probe_admin_settings_submit() {

  drupal_set_message('Clearing menu cache');

  //Rebuild the menu system
  menu_rebuild();
}
